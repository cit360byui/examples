package edu.byui.cit.milliseconds;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Date;


public class MainActivity extends AppCompatActivity {
	private EditText mltMillis, mltDateTime;
	private TextWatcher millisHandler, dateTimeHandler;
	private final NumberFormat numberFmtr;
	private final DateFormat dateTimeFmtr;
	private final DateFormat[] dateFmtrs;


	public MainActivity() {
		super();
		numberFmtr = NumberFormat.getNumberInstance();
		dateTimeFmtr = DateFormat.getDateTimeInstance();
		DateFormat mediumDateFmtr = DateFormat.getDateInstance(
				DateFormat.MEDIUM);
		DateFormat shortDateFmtr =
				DateFormat.getDateInstance(DateFormat.SHORT);
		dateFmtrs = new DateFormat[]{
				dateTimeFmtr, mediumDateFmtr, shortDateFmtr
		};
	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		millisHandler = new HandleMillis();
		dateTimeHandler = new HandleDateTime();

		mltMillis = findViewById(R.id.mltMillis);
		mltDateTime = findViewById(R.id.mltDateTime);
		mltMillis.addTextChangedListener(millisHandler);
		mltDateTime.addTextChangedListener(dateTimeHandler);

		mltMillis.requestFocus();
	}


	private final class HandleMillis implements TextWatcher {
		@Override
		public void beforeTextChanged(CharSequence seq, int i, int i1,
				int i2) {
		}

		@Override
		public void onTextChanged(CharSequence seq, int i, int i1, int i2) {
		}

		@Override
		public void afterTextChanged(Editable editable) {
			mltDateTime.removeTextChangedListener(dateTimeHandler);
			mltDateTime.getText().clear();
			String text = editable.toString();
			String[] lines = text.split("\r?\n");
			for (String line : lines) {
				String str = null;
				try {
					long millis = numberFmtr.parse(line.trim()).longValue();
					Date date = new Date(millis);
					str = dateTimeFmtr.format(date) + "\n";
				}
				catch (ParseException ex) {
					str = "\n";
				}
				mltDateTime.append(str);
			}
			mltDateTime.addTextChangedListener(dateTimeHandler);
		}
	}


	private final class HandleDateTime implements TextWatcher {
		@Override
		public void beforeTextChanged(CharSequence seq, int i, int i1,
				int i2) {
		}

		@Override
		public void onTextChanged(CharSequence seq, int i, int i1, int i2) {
		}

		@Override
		public void afterTextChanged(Editable editable) {
			mltMillis.removeTextChangedListener(millisHandler);
			mltMillis.getText().clear();
			String text = editable.toString();
			String[] lines = text.split("\r?\n");
			for (String line : lines) {
				line = line.trim();

				Date date = null;
				for (DateFormat fmtr : dateFmtrs) {
					try {
						date = fmtr.parse(line);
						if (date != null) {
							break;
						}
					}
					catch (ParseException ignored) {
					}
				}

				String str = "\n";
				if (date != null) {
					long millis = date.getTime();
					str = Long.toString(millis) + "\n";
				}
				mltMillis.append(str);
			}
			mltMillis.addTextChangedListener(millisHandler);
		}
	}
}
