package edu.byui.cit.room.model;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;


@Entity
public class Creature {
	@PrimaryKey(autoGenerate = true)
	private long creatureKey;

	private String creatureName;
	private String creatureType;


	@Ignore
	public Creature(long creatureKey) {
		this.creatureKey = creatureKey;
	}

	@Ignore
	public Creature(String creatureName, String creatureType) {
		this.creatureName = creatureName;
		this.creatureType = creatureType;
	}

	public Creature(long creatureKey, String creatureName, String creatureType) {
		this.creatureKey = creatureKey;
		this.creatureName = creatureName;
		this.creatureType = creatureType;
	}

	public long getCreatureKey() {
		return creatureKey;
	}

	void setCreatureKey(long creatureKey) {
		this.creatureKey = creatureKey;
	}

	public String getCreatureName() {
		return creatureName;
	}

	public void setCreatureName(String creatureName) {
		this.creatureName = creatureName;
	}

	public String getCreatureType() {
		return creatureType;
	}

	public void setCreatureType(String creatureType) {
		this.creatureType = creatureType;
	}

	@Override
	public boolean equals(Object obj) {
		boolean eq = (this == obj);
		if (!eq && obj != null && getClass() == obj.getClass()) {
			Creature other = (Creature)obj;
			eq = this.creatureKey == other.creatureKey &&
					this.creatureName.equals(other.creatureName) &&
					this.creatureType.equals(other.creatureType);
		}
		return eq;
	}

	@Override
	public int hashCode() {
		return Objects.hash(creatureKey, creatureName, creatureType);
	}

	@NonNull
	@Override
	public String toString() {
		return creatureKey + "  " + creatureName + " " + creatureType;
	}
}
