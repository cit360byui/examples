package edu.byui.cit.room.model;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;


@Dao
public abstract class CreatureDAO {
	@Query("SELECT COUNT(*) FROM Creature")
	public abstract long count();

	@Query("SELECT * FROM Creature")
	public abstract List<Creature> getAllCreatures();

	@Query("SELECT * FROM Creature WHERE creatureKey = :creatureKey")
	public abstract Creature getCreatureByPrimaryKey(long creatureKey);

	public void insert(Creature creature) {
		long primaryKey = realInsert(creature);
		creature.setCreatureKey(primaryKey);
	}

	@Insert
	abstract long realInsert(Creature creature);

	@Update
	public abstract void update(Creature creature);

	@Delete
	public abstract void delete(Creature creature);

	@Query("DELETE FROM Creature")
	public abstract void deleteAll();
}
